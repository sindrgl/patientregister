package mappe.del2.patientregister;

import mappe.del2.patientregister.exception.AddException;
import mappe.del2.patientregister.exception.RemoveException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Testing class for Patient and PatientRegister class. Creates, adds, edit and removes patients.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class PatientRegisterTest {
    PatientRegister patientRegister = new PatientRegister();

    @Nested
    public class CreatePatientsTest {

        @Test
        @DisplayName("Creates normal person does not throw exception")
        public void createPatient() {
            assertDoesNotThrow(() -> new Patient("12345678910","Sindre","Glomnes"));
        }

        @Test
        @DisplayName("Creates person without firstname throws exception")
        public void createPatientNoFirstName() {
            assertThrows(IllegalArgumentException.class, () -> new Patient("12345678910","","Glomnes"));
        }

        @Test
        @DisplayName("Creates person without lastname throws exception")
        public void createPatientNoLastName() {
            assertThrows(IllegalArgumentException.class, () -> new Patient("12345678910","Sindre",""));
        }

        @Test
        @DisplayName("PSNumber without 11 digits throws exception")
        public void createPatientPSNumberNot11Digits() {
            assertThrows(IllegalArgumentException.class, () -> new Patient("12345678","Sindre","Glomnes"));
        }

        @Test
        @DisplayName("PSNumber not only digits throws exception")
        public void createPatientPSNumberNotOnlyDigits() {
            assertThrows(IllegalArgumentException.class, () -> new Patient("a2345678910","Sindre","Glomnes"));
        }
    }

    @Nested
    public class SetPatientTest {

        @Test
        @DisplayName("Sets variable of a patient does not throw exception")
        public void setters() {
            Patient sindre = new Patient("12345678910", "Sindre", "Glomnes");
            assertDoesNotThrow(() -> sindre.setFirstName("Sondre"));
            assertDoesNotThrow(() -> sindre.setLastName("Glooomnes"));
            assertDoesNotThrow(() -> sindre.setPersonalSecurityNumber("98765432101"));
            assertDoesNotThrow(() -> sindre.setDiagnosis("Syk"));
            assertDoesNotThrow(() -> sindre.setGeneralPractitioner("Lisa"));
        }

        @Test
        @DisplayName("Sets first name to null throws exception")
        public void illegalFirstName() {
            Patient sindre = new Patient("12345678910", "Sindre", "Glomnes");
            assertThrows(IllegalArgumentException.class, () -> sindre.setFirstName(""));
        }

        @Test
        @DisplayName("Sets last name to null throws exception")
        public void illegalLastName() {
            Patient sindre = new Patient("12345678910", "Sindre", "Glomnes");
            assertThrows(IllegalArgumentException.class, () -> sindre.setLastName(""));
        }

        @Test
        @DisplayName("Sets PSNumber not 11 digits throws exception")
        public void PSNumberNot11Digits() {
            Patient sindre = new Patient("12345678910", "Sindre", "Glomnes");
            assertThrows(IllegalArgumentException.class, () -> sindre.setPersonalSecurityNumber("123456"));
        }

        @Test
        @DisplayName("Sets PSNumber to not only digits throws exception")
        public void PSNumberNotDigits() {
            Patient sindre = new Patient("12345678910", "Sindre", "Glomnes");
            assertThrows(IllegalArgumentException.class, () -> sindre.setPersonalSecurityNumber("abc45678910"));
        }

    }

    @Nested
    public class AddPatientTest {

        @Test
        @DisplayName("Can add a patient")
        public void addPatient() {
            assertDoesNotThrow(() -> patientRegister.addPatient(new Patient("12345678910","Sindre","Glomnes")));
        }

        @Test
        @DisplayName("Cannot add a duplicate patient")
        public void addDuplicatePatient() throws AddException {
            patientRegister.addPatient(new Patient("12345678910","Sindre","Glomnes"));
            assertThrows(AddException.class, () -> patientRegister.addPatient(new Patient("12345678910","Sindre","Glomnes")));
        }

        @Test
        @DisplayName("Cannot add patient without a first name")
        public void addPatientNoFirstName() {
            assertThrows(IllegalArgumentException.class, () -> patientRegister.addPatient(new Patient("1234","","Glomnes")));
        }

        @Test
        @DisplayName("Cannot add patient without a last name")
        public void addPatientNoLastName() {
            assertThrows(IllegalArgumentException.class, () -> patientRegister.addPatient(new Patient("1234","Sindre","")));
        }

        @Test
        @DisplayName("Cannot add patient with a personal security number not 11 digits")
        public void addPatientPSNDoesNotHave11Digits() {
            assertThrows(IllegalArgumentException.class, () -> patientRegister.addPatient(new Patient("1234","Sindre","Glomnes")));
        }
        @Test
        @DisplayName("Cannot add patient, personal security number does not only contain digits")
        public void addPatientPSNIsNotDigits() {
            assertThrows(IllegalArgumentException.class, () -> patientRegister.addPatient(new Patient("a2345678910","Sindre","Glomnes")));
        }

    }

    @Nested
    public class RemovePatientTest {

        @Test
        @DisplayName("Can remove a patient")
        public void removePatient() throws AddException {
            patientRegister.addPatient(new Patient("12345678910","Sindre","Glomnes"));
            assertDoesNotThrow(() -> patientRegister.removePatient(new Patient("12345678910","Sindre","Glomnes")));
        }

        @Test
        @DisplayName("Cannot remove a non existing patient")
        public void removeNonExsistingPatient() {
            assertThrows(RemoveException.class, () -> patientRegister.removePatient(new Patient("12345678910","Sindre","Glomnes")));
        }
    }

}
