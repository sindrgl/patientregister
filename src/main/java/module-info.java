module patientregister {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires opencsv;

    opens mappe.del2.patientregister.controll to javafx.graphics;
    opens mappe.del2.patientregister.ui to javafx.graphics;
    opens mappe.del2.patientregister to javafx.base;
}