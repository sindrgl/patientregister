package mappe.del2.patientregister.exception;

/**
 * AddExceptions for adding a duplicates of a patient to
 * the patientRegister. The exception is catched and displayed in
 * error alert.
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class AddException extends Exception {
    public AddException(String errorMessage) {
        super(errorMessage);
    }
}
