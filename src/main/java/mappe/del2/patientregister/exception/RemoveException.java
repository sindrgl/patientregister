package mappe.del2.patientregister.exception;

/**
 * RemoveExceptions for deleting a patient not found
 * in the patientRegister. The exception is catched and displayed in
 * error alert.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class RemoveException extends Exception {
    public RemoveException(String errorMessage) {
        super(errorMessage);
    }
}
