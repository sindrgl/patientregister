package mappe.del2.patientregister.controll;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import mappe.del2.patientregister.percistence.FileManagement;
import mappe.del2.patientregister.Patient;
import mappe.del2.patientregister.PatientRegister;
import mappe.del2.patientregister.exception.AddException;
import mappe.del2.patientregister.exception.RemoveException;
import mappe.del2.patientregister.ui.Factory;
import mappe.del2.patientregister.ui.PatientDialog;
import java.io.FileNotFoundException;
import java.util.Optional;

/**
 * The main controller of the application. All interactions (action events) through
 * the GUI are sent to the MainController methods.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class MainController {

    private FileManagement fileManagement = new FileManagement(); //Used for import and export of CSV files.

    /**
     * Adds a patient to the patientRegister and updates the tableView.
     * Shows a error alert if the patient is a duplicate.
     *
     * @param patientRegister the register of patients
     * @param parent the Factory class.
     */
    public void addPatient(PatientRegister patientRegister, Factory parent) {
        PatientDialog dialog = new PatientDialog(this);
        Optional<Patient> result = dialog.showAndWait();

        if (result.isPresent()) {
            try {
                Patient newPatient = result.get();
                patientRegister.addPatient(newPatient);
            } catch (AddException e) {
                error(e.getMessage());
            }
            parent.updateObservableList();
        }
    }

    /**
     * Edits a patient in the patientRegister and updates the TableView.
     * If a patient from the tableView is not selected, a error
     * alert will be showed to the user.
     *
     * @param selectedPatient The patient to be edited.
     * @param parent the Factory class.
     */
    public void editPatient(Patient selectedPatient, Factory parent) {
        if (selectedPatient == null) {
            Alert notChosenPaitent = new Alert(Alert.AlertType.ERROR);
            notChosenPaitent.setTitle("Employee Details - Edit");
            notChosenPaitent.setHeaderText("Patient not selected");
            notChosenPaitent.setContentText("You must select a patient from the list to edit");
            notChosenPaitent.showAndWait();
        } else {
            PatientDialog employeeDialog = new PatientDialog(selectedPatient, this);
            employeeDialog.showAndWait();

            parent.updateObservableList();
        }
    }

    /**
     * Removes a patient from the patientRegister and updates the tableView.
     * If a patient from the tableView is not selected, a error
     * alert will be showed to the user.
     * If a patient is selected, a confirmation alert will be showed to the user.
     * If a nonexisting patient is selected (should not be possible), a
     * error alert will be showed to the user.
     *
     *
     * @param patientRegister The register of patients.
     * @param selectedPatient The patient to be removed.
     * @param parent The Factory class.
     */
    public void removePatient(PatientRegister patientRegister, Patient selectedPatient, Factory parent) {
        if (selectedPatient == null) {
            Alert notChosenPaitent = new Alert(Alert.AlertType.ERROR);
            notChosenPaitent.setTitle("Delete confirmation");
            notChosenPaitent.setHeaderText("Patient not selected");
            notChosenPaitent.setContentText("You must select a patient from the list to remove");
            notChosenPaitent.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete confirmation");
            alert.setHeaderText("Delete confirmation");
            alert.setContentText("Are you sure you want to delete this item?");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.isPresent() && (result.get() == ButtonType.OK)) {
                try {
                    patientRegister.removePatient(selectedPatient);
                } catch (RemoveException e) {
                    error(e.getMessage());
                }

            }
        }
        parent.updateObservableList();
    }

    /**
     * Imports a CSV file, adds the patients to the patient
     * register and updates the tableView. Updates the statusbar. Shows an alert if
     * something went wrong (file is not .csv).
     *
     * @param patientRegister The register of patients.
     * @param parent The Factory class.
     */
    public void importCsv(PatientRegister patientRegister, Factory parent) {
        try {
            patientRegister.clearList();
            for (Patient p: fileManagement.importFile(this)) {
                try{
                    patientRegister.addPatient(p);
                } catch (IllegalArgumentException e) {
                    error(e.getMessage());
                }
            }
            parent.updateObservableList();
            parent.updateStatusBar("Import successful");
        } catch (FileNotFoundException | AddException e) {
            error(e.getMessage());
        }
    }

    /**
     * Exports the data from the application to a CSV file.
     * Updates the statusbar. Shows an alert if something went wrong.
     *
     * @param patientRegisterListWrapper The list of patients.
     */
    public void exportCsv(ObservableList<Patient> patientRegisterListWrapper, Factory parent) {
        fileManagement.exportFile(patientRegisterListWrapper,this);
        parent.updateStatusBar("Export successful");

    }

    /**
     * Information alert about the application.
     */
    public void helpAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patients Register\nv0.1-SNAPSHOT");
        alert.setContentText("A brilliant application created by\n(C)Sindre Glomnes\n2021-04-25");
        alert.showAndWait();
    }

    /**
     * Error alert used to showcase exceptions caught,
     * such as importing wrong file, adding duplicate patient, etc.
     *
     * @param e The error message.
     */
    public void error(String e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Something went wrong...");
        alert.setHeaderText("Something went wrong!");
        alert.setContentText(e);
        alert.showAndWait();
    }

    /**
     * Confirmation alert for closing the application.
     */
    public void exitApplication() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Exit Application ?");
        alert.setContentText("Are you sure you want to exit this application?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && (result.get() == ButtonType.OK)) {
            Platform.exit();
        }
    }
}



