package mappe.del2.patientregister;

import mappe.del2.patientregister.controll.MainController;
import mappe.del2.patientregister.exception.AddException;
import mappe.del2.patientregister.exception.RemoveException;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents a register of patients. A patient can be added and removed from
 * the register.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class PatientRegister {
    private ArrayList<Patient> patients;

    /**
     * Creates a patient register.
     * Makes a arraylist for patients.
     */
    public PatientRegister() {
        this.patients = new ArrayList<>();
    }

    /**
     * Adds a patient to the patients arraylist.
     * Throws AddException if the patient is a duplicate
     * of a patient already in the list.
     *
     * @param patient the patient to be added.
     * @throws AddException if patient is duplicate.
     */
    public void addPatient(Patient patient) throws AddException {
        if (!patients.contains(patient)) {
            patients.add(patient);
        } else {
            throw new AddException("Patient is already in register");
        }
    }

    /**
     * Removes a patient from the patients arraylist.
     * Throws a RemoveException if the list does not
     * contain the patient.
     *
     * @param patient
     * @throws RemoveException
     */
    public void removePatient(Patient patient) throws RemoveException {
        if (patients.contains(patient)) {
            patients.remove(patient);
        } else {
            throw new RemoveException("Is not in register");
        }
    }

    /**
     * Returns the patients list.
     *
     * @return
     */
    public Collection<Patient> getPatients() {
        return patients;
    }

    /**
     * Removes all patients from the register.
     */
    public void clearList() {
        patients.clear();
    }
}