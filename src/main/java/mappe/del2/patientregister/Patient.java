package mappe.del2.patientregister;

import java.util.Objects;

/**
 * Class Patient represents an instance of a Patient. A patient
 * consists of a first name, last name and a personal security number. Diagnosis and
 * general practitioner can also be set to a patient. A patient can be edited by
 * changing its attributes.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class Patient {
    private String personalSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;


    /**
     * Constructor for adding a patient through the application.
     * A patient must have a first name, last name, and a
     * personal security number containing 11 digits
     * (Digits only).
     *
     * @param personalSecurityNumber
     * @param firstName
     * @param lastName
     * @throws IllegalArgumentException no firstname/lastname, security number is not 11 digits or
     * does not only contain digits.
     */
    public Patient(String personalSecurityNumber, String firstName, String lastName) throws IllegalArgumentException {
        if (!personalSecurityNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Personal security number can only contain digits");
        }
        if (firstName.equals("")) {
            throw new IllegalArgumentException("Patient must have a first name");
        }
        if (lastName.equals("")) {
            throw new IllegalArgumentException("Patient must have a last name");
        }
        if (personalSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Personal security number must have 11 digits");
        }

        this.personalSecurityNumber = personalSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Constructor used for importing .csv files.
     * personal security number containing 11 digits
     * (Digits only).
     *
     * @param firstName
     * @param lastName
     * @param generalPractitioner
     * @param personalSecurityNumber
     * @param diagnosis
     * @throws IllegalArgumentException no firstname/lastname, security number is not 11 digits or
     *      * does not only contain digits.
     */
    public Patient( String firstName, String lastName,String generalPractitioner, String personalSecurityNumber, String diagnosis ) throws IllegalArgumentException {
        if (!personalSecurityNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Personal security number can only contain digits");
        }
        if (personalSecurityNumber.equals("")) {
            throw new IllegalArgumentException("Patient must have a personal security number");
        }
        if (firstName.equals("")) {
            throw new IllegalArgumentException("Patient must have a first name");
        }
        if (lastName.equals("")) {
            throw new IllegalArgumentException("Patient must have a last name");
        }
        if (personalSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Personal security number must have 11 digits");
        }

        this.personalSecurityNumber = personalSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    public String getPersonalSecurityNumber() {
        return personalSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets the personal security number of a patient.
     * Must contain 11 digits, digits only.
     * @param personalSecurityNumber
     * @throws IllegalArgumentException
     */
    public void setPersonalSecurityNumber(String personalSecurityNumber) throws IllegalArgumentException{
        if (!personalSecurityNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Personal security number can only contain digits");
        }
        if (personalSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Personal security number must have 11 digits");
        }
        this.personalSecurityNumber = personalSecurityNumber;
    }

    /**
     * Sets the first name of a patient
     * first name cannot be null.
     * @param firstName
     * @throws IllegalArgumentException
     */
    public void setFirstName(String firstName) throws IllegalArgumentException {
        if (firstName.equals("")) {
            throw new IllegalArgumentException("Patient must have a first name");
        }
        this.firstName = firstName;
    }

    /**
     * Sets the last name of a patient
     * last name cannot be null.
     * @param lastName
     * @throws IllegalArgumentException
     */
    public void setLastName(String lastName) throws IllegalArgumentException {
        if (lastName.equals("")) {
            throw new IllegalArgumentException("Patient must have a last name");
        }
        this.lastName = lastName;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Checks if a patient object equals this patient.
     * They are equal if all parameters are equal.
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return personalSecurityNumber.equals(patient.personalSecurityNumber) &&
                Objects.equals(firstName, patient.firstName) &&
                Objects.equals(lastName, patient.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personalSecurityNumber, firstName, lastName, diagnosis, generalPractitioner);
    }

    /**
     * Informational String about the patient
     *
     * @return Information about the patient.
     */
    @Override
    public String toString() {
        return "Patient: " +
                "personalSecurityNumber= " + personalSecurityNumber +
                ", firstName= " + firstName +
                ", lastName= " + lastName +
                ", diagnosis= " + diagnosis +
                ", generalPractitioner= " + generalPractitioner;
    }
}
