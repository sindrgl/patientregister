package mappe.del2.patientregister.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import mappe.del2.patientregister.Patient;
import mappe.del2.patientregister.PatientRegister;
import mappe.del2.patientregister.controll.MainController;
/**
 * Creates instances of GUI-elements such as statusbar,
 * menus, buttons and tableView.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class Factory {
    private MainController mainController;
    private PatientRegister patientRegister;
    private ObservableList<Patient> patientRegisterListWrapper;
    private TableView<Patient> patientTableView;
    HBox statusBar = new HBox();

    /**
     * The constructor creates an instance of mainController and
     * patientRegister.
     */
    public Factory() {
        this.mainController = new MainController();
        this.patientRegister = new PatientRegister();
    }

    /**
     * Creates the tableView with firstName, lastName, personalSecurityNumber
     * and generalPractitioner.
     *
     * @return the TableView.
     */
    public TableView<Patient> createCentreContent() {
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("firstName");
        firstNameColumn.setMinWidth(200);
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("lastName");
        lastNameColumn.setMinWidth(200);
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Patient, Long> socialSecurityColumn = new TableColumn<>("personalSecurityNumber");
        socialSecurityColumn.setMinWidth(200);
        socialSecurityColumn.setCellValueFactory(new PropertyValueFactory<>("personalSecurityNumber"));

        TableColumn<Patient, String> generalPractitionerColumn = new TableColumn<>("generalPractitioner");
        generalPractitionerColumn.setMinWidth(200);
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));

        TableView<Patient> tableView = new TableView<>();

        ObservableList<Patient> observableList = this.getPatientTableView();

        tableView.setItems(observableList);
        tableView.getColumns().addAll(firstNameColumn,lastNameColumn,socialSecurityColumn,generalPractitionerColumn);

        this.patientTableView = tableView;
        return tableView;
    }

    /**
     * Adds the content of the patientRegister Arraylist to the
     * ObservableList patientRegisterListWrapper.
     * @return the ObservableList.
     */
    public ObservableList<Patient> getPatientTableView() {
        if (this.patientRegister == null) {
            patientRegisterListWrapper =  null;
        } else {
            patientRegisterListWrapper = FXCollections.observableArrayList(this.patientRegister.getPatients());

        }
        return patientRegisterListWrapper;
    }

    /**
     * Updates the ObservableList by setting its content to the
     * content of the patientRegister Arraylist.
     */
    public void updateObservableList() {
        this.patientRegisterListWrapper.setAll(this.patientRegister.getPatients());
    }


    /**
     * Creates a statusBar used to showcase the status of the application.
     *
     * @return The statusbar.
     */
    public Node createStatusBar() {
        statusBar.getChildren().add(new Text("Status: OK"));
        statusBar.setStyle("-fx-background-color: #999999;");

        return statusBar;
    }

    /**
     * Updates the current status for the statusbar.
     *
     * @param status the message for status
     */
    public void updateStatusBar(String status) {
        statusBar.getChildren().clear();
        statusBar.getChildren().add(new Text("Status: " + status));
    }

    /**
     * Creates the buttons for add, edit and remove patient.
     *
     * @return The buttons.
     */
    public AnchorPane createButtons() {
        AnchorPane buttons = new AnchorPane();

        Button addButton = new Button();
        Button removeButton = new Button();
        Button editButton = new Button();

        ImageView addImage = new ImageView(new Image("file:src/main/resources/images/add.png"));
        addImage.setFitHeight(50);
        addImage.setPreserveRatio(true);

        ImageView removeImage = new ImageView(new Image("file:src/main/resources/images/remove.png"));
        removeImage.setFitHeight(50);
        removeImage.setPreserveRatio(true);

        ImageView editImage = new ImageView(new Image("file:src/main/resources/images/edit.png"));
        editImage.setFitHeight(50);
        editImage.setPreserveRatio(true);

        addButton.setGraphic(addImage);
        removeButton.setGraphic(removeImage);
        editButton.setGraphic(editImage);

        addButton.setLayoutX(10);
        addButton.setLayoutY(10);
        addButton.setPrefWidth(60);
        addButton.setPrefHeight(60);

        removeButton.setLayoutX(80);
        removeButton.setLayoutY(10);
        removeButton.setPrefWidth(60);
        removeButton.setPrefHeight(60);

        editButton.setLayoutX(150);
        editButton.setLayoutY(10);
        editButton.setPrefWidth(60);
        editButton.setPrefHeight(60);

        addButton.setOnAction(event ->
                mainController.addPatient(this.patientRegister, this));

        removeButton.setOnAction(event ->
            mainController.removePatient(patientRegister, this.patientTableView.getSelectionModel().getSelectedItem(), this));

        editButton.setOnAction(event ->
                mainController.editPatient(this.patientTableView.getSelectionModel().getSelectedItem(), this));

        buttons.getChildren().addAll(addButton, removeButton, editButton);

        return buttons;
    }

    /**
     * Creates the menu containing File, Edit and help.
     *
     * @return the menuBar.
     */
    public MenuBar createMenus() {

        //Files
        Menu menuFile = new Menu("File");

        MenuItem importFrom = new MenuItem("Import from .CSV...");
        importFrom.setOnAction(event -> mainController.importCsv(patientRegister,this));

        MenuItem exportTo = new MenuItem("Export to .CSV...");
        exportTo.setOnAction(event -> mainController.exportCsv(patientRegisterListWrapper,this));

        SeparatorMenuItem seperator = new SeparatorMenuItem();
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(event -> mainController.exitApplication());
        menuFile.getItems().addAll(importFrom,exportTo,seperator,exit);

        //Edit
        Menu menuEdit = new Menu("Edit");

        MenuItem add = new MenuItem("Add new Patient...");
        MenuItem remove = new MenuItem("Remove Selected Patient");
        MenuItem edit = new MenuItem("Edit Selected Patient");
        add.setOnAction(event ->
                mainController.addPatient(this.patientRegister, this));

        remove.setOnAction(event ->
            mainController.removePatient(patientRegister, this.patientTableView.getSelectionModel().getSelectedItem(), this));

        edit.setOnAction(event ->
                mainController.editPatient(this.patientTableView.getSelectionModel().getSelectedItem(), this));

        menuEdit.getItems().addAll(add,edit,remove);

        //Help
        Menu menuHelp = new Menu("Help");

        MenuItem about = new MenuItem("About");
        about.setOnAction(event -> mainController.helpAbout());
        menuHelp.getItems().addAll(about);

        //MenuBar
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(menuFile,menuEdit,menuHelp);
        return menuBar;
    }

}
