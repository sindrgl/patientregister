package mappe.del2.patientregister.ui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * Main class. Sets the stage with elements from Factory class,
 * and shows the stage.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class PatientRegisterApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Factory factory = new Factory();

        // Icon for the application
        primaryStage.getIcons().add(new Image("file:src/main/resources/images/register.png"));

        BorderPane root = new BorderPane();
        VBox topContainer = new VBox();

        // Place the menubar in the topContainer
        topContainer.getChildren().addAll(factory.createMenus(), factory.createButtons());
        topContainer.setMinHeight(105);

        // Place the top container in the top-section of the BorderPane
        root.setTop(topContainer);

        // Place the StatusBar at the bottom
        root.setBottom(factory.createStatusBar());

        // Place the table view in the centre
        root.setCenter(factory.createCentreContent());

        // Create the scene, adds the BorderPane root.
        Scene scene = new Scene(root, 820, 500);

        // Set title of the stage and adds the scene
        primaryStage.setTitle("Patient Register v0.1-SNAPSHOT");
        primaryStage.setScene(scene);

        // Opens the stage for the user.
        primaryStage.show();
    }

    /**
     * Exits the application
     */
    @Override
    public void stop() {
        System.exit(0);
    }
    
}
