package mappe.del2.patientregister.ui;

import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import mappe.del2.patientregister.Patient;
import mappe.del2.patientregister.controll.MainController;

/**
 * Creates a dialog window used for adding and editing patients.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class PatientDialog extends Dialog<Patient> {

    /**
     * Modes for new patient and edit patient.
     *
     */
    private enum Mode {
        NEW, EDIT
    }

    private final Mode mode;

    private Patient existingPatient = null;
    MainController main;


    /**
     * Constructor for new patient.
     * sets the mode to NEW and creates the patient.
     */
    public PatientDialog(MainController main) {
        super();
        this.mode = Mode.NEW;
        this.main = main;
        // Create the content of the dialog
        createPatient();

    }

    /**
     * Constructor for editing patient.
     * Sets the mode to EDIT.
     *
     * @param patient the patient to be edited.
     */
    public PatientDialog(Patient patient, MainController main) {
        super();
        this.mode = Mode.EDIT;
        this.main = main;
        this.existingPatient = patient;
        // Create the content of the dialog
        createPatient();
    }


    /**
     * Opens dialog for creating or editing patient
     * based on mode NEW or EDIT.
     */
    private void createPatient() {
        //Sets the title of the dialogPane based on mode.
        switch (this.mode) {
            case EDIT:
                setTitle("Employee Details - Edit");
                break;

            case NEW:
                setTitle("Employee Details - Add");
                break;

            default:
                setTitle("Employee Details - UNKNOWN MODE...");
                break;
        }

        //Sets the layout of the dialogPane
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField firstNameField = new TextField();
        firstNameField.setPromptText("First name");
        Label firstNameLabel = new Label("First name:");

        TextField lastNameField = new TextField();
        lastNameField.setPromptText("last name");
        Label lastNameLabel = new Label("Last name:");

        TextField personalSecurityField = new TextField();
        personalSecurityField.setPromptText("Personal Security number");
        Label personalSecurityLabel = new Label("Personal Security number:");

        grid.add(firstNameLabel, 0, 0);
        grid.add(firstNameField, 1, 0);
        grid.add(lastNameLabel, 0, 1);
        grid.add(lastNameField, 1, 1);
        grid.add(personalSecurityLabel, 0, 2);
        grid.add(personalSecurityField, 1, 2);

        // Fills the textfield with a patients parameters, if the mode is EDIT
        if (mode == Mode.EDIT) {
            firstNameField.setText(existingPatient.getFirstName());
            lastNameField.setText(existingPatient.getLastName());
            personalSecurityField.setText(existingPatient.getPersonalSecurityNumber());
        }

        getDialogPane().setContent(grid);


        setResultConverter((ButtonType button) -> {
            Patient result = null;
            if (button == ButtonType.OK) {
                if (mode == Mode.NEW) {
                    try {
                        result = new Patient(personalSecurityField.getText(),firstNameField.getText(),lastNameField.getText());
                    } catch (Exception e) {
                        main.error(e.getMessage());
                       }
                } else if (mode == Mode.EDIT) {
                    try {
                        existingPatient.setPersonalSecurityNumber(personalSecurityField.getText());
                        existingPatient.setFirstName(firstNameField.getText());
                        existingPatient.setLastName(lastNameField.getText());
                        result = existingPatient;
                    } catch (Exception e) {
                        main.error(e.getMessage());
                    }
                }
            }
            return result;
        });
    }
}

