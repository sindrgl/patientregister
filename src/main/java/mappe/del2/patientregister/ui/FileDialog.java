package mappe.del2.patientregister.ui;


import javafx.scene.control.*;
import java.util.Optional;

/**
 * Shows dialogs for import and export of .csv files.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class FileDialog {

    /**
     * When user chooses a file not .csv, an alert will be shown
     * asking to select new file or cancel operation.
     *
     * @return true = chose new file, false = cancel.
     */
    public boolean notValid() {
        ButtonType select = new ButtonType("Select file", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.WARNING,
                "The chosen file type is not valid.\n" +
                        "Please choose another file\nclicking on Select file or Cancel the operation",
                select,
                cancel);

        alert.setTitle("File Details - Chosen file not valid");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && (result.get() == cancel)) {
            alert.close();
            return true;
        } else return !result.isPresent() || (result.get() != select);
    }

    /**
     * alert for overwriting a file.
     * Not used because Windows OS contains a inbuilt alert system for
     * the problem.
     *
     * @return true = overwrite, false = cancel.
     */
    public boolean overWrite() {
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.WARNING,
                "A file with same name exists. Do you want to overwrite it?",
                yes,
                no);

        alert.setTitle("File Details - A file with same name exists");

        Optional<ButtonType> result = alert.showAndWait();


        if (result.isPresent() && (result.get() == yes)) {
            alert.close();
            return true;
        } else if (result.isPresent() && (result.get() == no)){
            return false;
        }
        return true;
    }
}
