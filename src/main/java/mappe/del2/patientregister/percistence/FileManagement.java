package mappe.del2.patientregister.percistence;

import com.opencsv.CSVReader;
import javafx.collections.ObservableList;
import javafx.stage.FileChooser;
import mappe.del2.patientregister.Patient;
import mappe.del2.patientregister.controll.MainController;
import mappe.del2.patientregister.ui.FileDialog;
import java.io.*;
import java.util.ArrayList;

/**
 * Manages files. Used for importing and exporting of
 * CSV files.
 *
 * @author Sindre Glomnes
 * @version 2021.05.04
 */
public class FileManagement {
    private ArrayList<Patient> patientList = new ArrayList<>();
    private FileDialog dialog = new FileDialog();
    private FileChooser fileChooser;
    private File file;
    private FileReader reader;
    private CSVReader csvReader;
    MainController main;

    /**
     * Imports a CSV file and returns its content as
     * an arraylist of patients.
     * If the chosen file is not .csv, a warning alert will
     * be showed to the user.
     *
     * @param main MainController
     * @return ArrayList of patients from the .csv file.
     * @throws FileNotFoundException
     */
    public ArrayList<Patient> importFile(MainController main) throws FileNotFoundException {
        this.main = main;
        fileChooser = new FileChooser();
        file = fileChooser.showOpenDialog(null);

        while (!(String.valueOf(file).contains(".csv"))) {
            try {
                if (dialog.notValid()) {
                    return null;
                }
            } catch (Exception ignored) {}

            file = fileChooser.showOpenDialog(null);
        }

        reader = new FileReader(file);
        csvReader = new CSVReader(reader);
        patientList.clear();

        try {
            String[] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                for (var e: nextLine){
                    String[] patient = e.split(";");
                    try {
                        patientList.add(new Patient(patient[0], patient[1],patient[2],patient[3], ""));
                    } catch (IllegalArgumentException i) {
                        main.error(i.getMessage());
                    }
                }
            }
        } catch (IOException e) {
            main.error(e.getMessage());
        }
        return patientList;
    }

    /**
     * Exports the content of the application to a .csv file to
     * a chosen location on the computer.
     * If the file has a duplicate name, a overwrite warning
     * will be showed to the user.
     *
     * @param main MainController
     * @param patients The list of patients.
     */
    public void exportFile(ObservableList<Patient> patients, MainController main) {
        this.main = main;
        try {
            fileChooser = new FileChooser();
            fileChooser.setInitialFileName("newFile.csv");
            file = fileChooser.showSaveDialog(null);

            PrintWriter printWriter = new PrintWriter(file);
            StringBuilder sb = new StringBuilder();
            for (Patient p: patients) {
                sb.append(p.getFirstName());
                sb.append(";");
                sb.append(p.getLastName());
                sb.append(";");
                sb.append(p.getGeneralPractitioner());
                sb.append(";");
                sb.append(p.getPersonalSecurityNumber());
                sb.append("\n");
            }
            printWriter.write(sb.toString());
            printWriter.close();
        } catch (Exception ignore) {}
    }
}
